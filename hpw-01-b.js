//____________________________________________INICIO________________________________________________//

var Grupo = function(_c,_n,_d,_m,_a){
  if(!_c){
    return{"exito":false,"mensaje":"La clave de la Materia no ha sido definida"};
  }
  return{"clave":_c,"nombre":_n,"docente":_d,"materia":_m,"alumno":_a};
};

var Docente = function(_c,_n,_a,_ga){
  if(!_c){
    return{"exito":false,"mensaje":"La clave del Docente no ha sido definida"};
  }
	return{"clave":_c,"nombre":_n,"apellido":_a,"grado_acadenico":_ga};
};

var Alumno = function(_c,_n,_a,_ca){
  if(!_c){
    return{"exito":false,"mensaje":"La clave del Alumno no ha sido definida"};
  }
	return{"clave":_c,"nombre":_n,"apellido":_a,"calificacion":_ca};
};

var Materia = function(_c,_n){
  if(!_c){
    return{"exito":false,"mensaje":"La clave de la Materia no ha sido definida"};
  }
	return{"clave":_c,"nombre":_n};
};

//____________________________________________MATERIA_______________________________________________//

//Funcion para agregar materias a un grupo
function createMateriaGrupo(g,m){
  if(g.materia===undefined){
    g.materia=[];
  }
  if((existeMateriaEnGrupo(g,m.clave))==-1){
    g.materia.push(m);   
    console.log("La materia "+m.nombre+" ha sido agregada al grupo "+g.nombre);
  }
  else{
  console.log("El grupo "+g.nombre+" ya cuenta con la materia "+m.nombre);
  }
}
//Funcion que servira para verificar si existe una materia en un grupo
function existeMateriaEnGrupo(g,c){
  if(!g.materia && g.materia.length===0){
    return -1;
  }
  for(var i=0;i<g.materia.length;i++){
    if(g.materia[i].clave===c){
      return i;
    }
  }
  return -1;
}
//Funcion para listar todos las materias que existen en un grupo
function readMateriaGrupo(g){
  if(g.materia===undefined){
    console.log("No hay materias asignadas al grupo "+g.nombre);
  }
  else{
    for(var i=0;i<g.materia.length;i++){
      console.log(g.materia[i].clave+"\t",g.materia[i].nombre);
    }
  }
}
//Funcion para eliminar una materia que este asignada a un grupo
function deleteMateriaGrupo(g,m){
  var x = existeMateriaEnGrupo(g,m.clave);
  if(x>=0){
    g.materia.splice(x,1);
    console.log("La materia "+m.nombre+" fue eliminada del grupo "+g.nombre+" exitosamente");
  }
  else{
    console.log("La materia "+m.nombre+" no esta asignada al grupo "+g.nombre+", verifique!");
  }
}
//Funcion para modificar los datos de la materia de un grupo
function updateMateriaGrupo(g,m){
  var x = existeMateriaEnGrupo(g,m.clave);
  if(x>=0){
    g.materia[x].nombre= m.nombre;
    console.log("La materia fue modificada exitosamente");
  }else{
    console.log("La materia no existe");
  }
}

//____________________________________________DOCENTE_______________________________________________//

//Funcion para agregar el docente correspondiente a un grupo
function createDocenteGrupo(g,d){
  if(g.docente===undefined){
    g.docente=d;
    console.log("El docente ha sido asignado al grupo "+g.nombre);
  }
  else{
    console.log("El grupo "+g.nombre+" ya cuenta con docente");
  }
}
//Funcion para listar los datos del docente de un grupo
function readDocenteGrupo(g){
  if(g.docente===undefined){
    console.log("El grupo "+g.nombre+" no cuenta con docente asignado");
  }
  else{
  console.log(g.docente.clave+"\t",g.docente.nombre);
  }
}
//Funcion para eliminar al docente  que este asignado a un grupo
function deleteDocenteGrupo(g){
  if(g.docente===undefined){
    console.log("El grupo "+g.nombre+" no tiene asignado docente");
  }
  else{
    g.docente=undefined;
    console.log("El docente asignado al grupo "+g.nombre+" fue eliminado exitosamente");
  }
}
//Funcion para modificar los datos de un docente
function updateDocenteGrupo(g,d){
  if(g.docente!=undefined){
    g.docente= d;
    console.log("La materia "+g.nombre+" fue modificada exitosamente");
  }else{
    console.log("La materia no existe");
  }
}

//____________________________________________ALUMNO________________________________________________//

//Funcion para agregar un alumno a un grupo
function createAlumnoGrupo(g,a){
  if(g.alumno===undefined){
    g.alumno=[];
  }
  if((existeAlumnoEnGrupo(g,a.clave))==-1){
    g.alumno.push(a);   
    console.log("El alumno "+a.nombre+" ha sido agregada al grupo "+g.nombre);
  }
  else{
  console.log("El grupo "+g.nombre+" ya cuenta con el alumno "+a.nombre);
  }
}
//Funcion que servira para verificar si existe un alumno en un grupo
function existeAlumnoEnGrupo(g,a){
  if(!g.alumno && g.alumno.length===0){
    return -1;
  }
  for(var i=0;i<g.alumno.length;i++){
    if(g.alumno[i].clave===a){
      return i;
    }
  }
  return -1;
}
//Funcion para listar los datos de los alumnos pertenecientes a un grupo
function readAlumnoGrupo(g){
  if(g.alumno===undefined){
    console.log("No hay alumnos asignadas al grupo "+g.nombre);
  }
  else{
    for(var i=0;i<g.alumno.length;i++){
          console.log(g.alumno[i].clave+"\t",g.alumno[i].nombre);
    }
  }
}
//Funcion para modificar los datos de un alumno de un grupo
function updateAlumnoGrupo(g,a){
  var x = existeAlumnoEnGrupo(g,a.clave);
  if(x>=0){
    g.alumno[x].nombre= a.nombre;
    g.alumno[x].apellido= a.apellido;
    g.alumno[x].calificacion= a.calificacion;
    console.log("El alumno con clave "+a.clave+" fue modificado exitosamente");
  }else{
    console.log("La materia no existe");
  }
}
//Funcion para eliminar el alumno que este asignado a un grupo
function deleteAlumnoGrupo(g,a){
  var x = existeAlumnoEnGrupo(g,a.clave);
  if(x>=0){
    g.alumno.splice(x,1);
    console.log("el alumno "+a.nombre+" fue eliminada del grupo "+g.nombre+" exitosamente");
  }
  else{
    console.log("El alumno "+a.nombre+" no esta asignada al grupo "+g.nombre+", verifique!");
  }
}

//_______________________________________________PRUEBAS____________________________________________//

var grupo1=Grupo('g1','n1');
var grupo2=Grupo('g2','n2');
var materia1=Materia('m1','n1');
var materia2=Materia('m2','n2');
var materia3=Materia('m2','n3');
var docente1=Docente('d1','n1','a1','ga1');
var docente2=Docente('d2','n2','a2','ga2');
var alumno1=Alumno('a1','n1','ape1');
var alumno2=Alumno('a2','n2','ape2');
var alumno3=Alumno('a2','n3','ape3');

createMateriaGrupo(grupo1,materia1);
createMateriaGrupo(grupo1,materia2);
createDocenteGrupo(grupo1,docente1);
createAlumnoGrupo(grupo1,alumno1);
createAlumnoGrupo(grupo1,alumno2);
readMateriaGrupo(grupo1);
readDocenteGrupo(grupo1);
readAlumnoGrupo(grupo1);
deleteMateriaGrupo(grupo1,materia1);
deleteDocenteGrupo(grupo1);
deleteAlumnoGrupo(grupo1,alumno1);
updateMateriaGrupo(grupo1,materia3);
updateAlumnoGrupo(grupo1,alumno3);
updateDocenteGrupo(grupo1,docente2);